//
//  ViewControllerGetPairNumbers.swift
//  ShowPair_Numbers
//
//  Created by Carlos Aguirre on 27/11/18.
//  Copyright © 2018 Carlos Aguirre. All rights reserved.
//

import Foundation
import UIKit

class ViewControllerGetPairNumbers: UIViewController{
    
    
    @IBOutlet weak var numberInsertedTextField: UITextField!
    
    @IBOutlet weak var numbersObtainedLabel: UILabel!
    
    @IBAction func getEvenNumbers(_ sender: Any) {
        
        let numberInserted:Int! = Int(numberInsertedTextField.text!)
        
        var numbersObtained:[Int]=[]
        
        var i = 1

        while i <= numberInserted {
            
            numbersObtained.append(i*2)
            i=i+1
            
        }
        
        numbersObtainedLabel.text=numbersObtained.description
        
    }
    
    @IBAction func exitButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
