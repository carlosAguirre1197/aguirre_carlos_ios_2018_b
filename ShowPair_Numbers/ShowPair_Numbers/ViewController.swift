//
//  ViewController.swift
//  ShowPair_Numbers
//
//  Created by Carlos Aguirre on 27/11/18.
//  Copyright © 2018 Carlos Aguirre. All rights reserved.
//

import UIKit
import FirebaseAuth

class ViewController: UIViewController {
    
   
    @IBOutlet weak var usernameTextField: UITextField!
    
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }


    @IBAction func loginButton(_ sender: Any) {
    
        let username = usernameTextField.text!
        
        let password = passwordTextField.text!
        
        Auth.auth().signIn(withEmail: username, password: password) { (data, error) in
            
            if let err = error {
                print (err)
                return
            }
            self.performSegue(withIdentifier: "loginSegue", sender: self)
        }
        
        
    }
    
    
    
}

